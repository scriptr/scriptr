# Scriptr #

This project is your directory of useful command line scripts.

## Installation ##

Run the following commands in shell:

    # go to directory where you will install the project
    cd ~/projects

    # install empty script repository
    composer create-project scriptr/scriptr:1.2.*

Add `vendor/bin` of installed project to system PATH. On Linux, add the following line to the end of `~/.bash_rc`:

    PATH="$PATH:~/projects/scriptr/vendor/bin"

Reopen command line shell.

## Using Scripts ##

Initially your script directory `vendor/bin` is almost empty.
 
Some scripts come preinstalled, such as `scriptr_create-package`, others require installation of additional Composer packages. 

Please check [script directory](https://bitbucket.org/scriptr/lib/src/master/doc/02-script-directory/) for full documentation on available scripts and how to install them.

## Customizing Scripts ##

If script requires customization to fit your needs better, you can easily copy and customize it. 

Read more on [adding your own scripts](https://bitbucket.org/scriptr/lib/src/master/doc/01-user-guide/02-creating-scripts.md).

## License ##

This project is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).